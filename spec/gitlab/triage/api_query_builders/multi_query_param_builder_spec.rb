require 'spec_helper'

require 'gitlab/triage/api_query_builders/multi_query_param_builder'

describe Gitlab::Triage::APIQueryBuilders::MultiQueryParamBuilder do
  let(:multi_params) do
    {
      'labels' => {
        param_content: %w[label1 label2],
        separator: ','
      }
    }
  end

  describe '#initialize' do
    context 'when param value is not allowed' do
      it 'raises an error if given value is not allowed' do
        expect { described_class.new('labels', 42, ',', allowed_values: %w[label1 label2]) }
          .to raise_error(Gitlab::Triage::ParamsValidator::InvalidParameter, /must be of type String/)
        expect { described_class.new('labels', %w[label1 label3], ',', allowed_values: %w[label1 label2]) }
          .to raise_error(Gitlab::Triage::ParamsValidator::InvalidParameter, /must be one of ['label1', 'label2']/)
      end
    end
  end

  describe '#build_param' do
    it 'builds the correct search string' do
      multi_params.each do |k, v|
        builder = described_class.new(k, v[:param_content], v[:separator])

        label1 = v.dig(:param_content, 0)
        label2 = v.dig(:param_content, 1)

        expect(builder.build_param)
          .to eq("&#{k}=#{label1}#{v[:separator]}#{label2}")
      end
    end

    context 'with leading or trailing whitespaces' do
      let(:multi_params) do
        {
          'labels' => {
            param_content: ["\nlabel1\n", "\n label2\n "],
            separator: ','
          }
        }
      end

      it 'strips the leading and trailing whitespaces' do
        multi_params.each do |k, v|
          builder = described_class.new(k, v[:param_content], v[:separator])

          label1 = v.dig(:param_content, 0).strip
          label2 = v.dig(:param_content, 1).strip

          expect(builder.build_param)
            .to eq("&#{k}=#{label1}#{v[:separator]}#{label2}")
        end
      end
    end
  end
end
