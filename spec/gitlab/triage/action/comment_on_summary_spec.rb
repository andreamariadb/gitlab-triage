require 'spec_helper'

require 'gitlab/triage/action/comment_on_summary'
require 'gitlab/triage/policies/rule_policy'
require 'gitlab/triage/policies_resources/rule_resources'

require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action::CommentOnSummary do
  include_context 'with network context'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ]
  end

  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      'issues',
      { name: 'Test summarize rule', actions: actions_hash },
      Gitlab::Triage::PoliciesResources::RuleResources.new(resources),
      network
    )
  end

  let(:actions_hash) do
    {
      comment_on_summary: template
    }
  end
  let(:template) { '# [{{title}}]({{web_url}})' }

  subject do
    described_class.new(
      policy: policy, network: network)
  end

  before do
    policy.summary = { 'project_id': 'SOME-ID', 'iid': 'SOME-IID' }.with_indifferent_access
  end

  describe '#act' do
    it 'posts comments to the summary issue' do
      stub_post_1 = stub_api(
        :post,
        "http://test.com/api/v4/projects/SOME-ID/issues/SOME-IID/notes",
        body: { body: '# [Issue #0](http://example.com/0)' }
      )
      stub_post_2 = stub_api(
        :post,
        "http://test.com/api/v4/projects/SOME-ID/issues/SOME-IID/notes",
        body: { body: '# [Issue #1](http://example.com/1)' }
      )

      subject.act

      assert_requested(stub_post_1)
      assert_requested(stub_post_2)
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end
  end
end
