require 'spec_helper'

require 'gitlab/triage/engine'
require 'gitlab/triage/network_adapters/test_adapter'

describe Gitlab::Triage::Engine do
  let(:token) { 'token' }
  let(:policies) { {} }

  let(:options) do
    Gitlab::Triage::Options.new.tap do |result|
      result.token = token
      result.source_id = 42
    end
  end

  subject do
    described_class.new(
      policies: policies,
      options: options,
      network_adapter_class: Gitlab::Triage::NetworkAdapters::TestAdapter)
  end

  describe '#initialize' do
    describe 'mandatory params' do
      context 'without all-projects flag or source-id given' do
        before do
          options.all = nil
          options.source_id = nil
        end

        it { expect { subject }.to raise_error(ArgumentError, 'A project_id is needed (pass it with the `--source-id` option)!') }
      end

      context 'with both all-projects flag and source-id given' do
        before do
          options.all = true
          options.source_id = 42
        end

        it {
          expect { subject }.to raise_error(ArgumentError, '--all-projects option cannot be used in conjunction with --source and --source-id option!')
        }
      end

      context 'with no token given' do
        before do
          options.token = nil
        end

        it { expect { subject }.to raise_error(ArgumentError, 'A token is needed (pass it with the `--token` option)!') }
      end
    end

    describe 'default values' do
      it 'sets default values for host_url, api_version, and per_page' do
        expect(subject.options.host_url).to eq('https://gitlab.com')
        expect(subject.options.api_version).to eq('v4')
        expect(subject.per_page).to eq(100)
      end

      context 'with host url in policies file' do
        let(:policies) do
          {
            host_url: 'https://labgit.com'
          }
        end

        it { expect(subject.options.host_url).to eq('https://labgit.com') }
      end

      it 'sets options.dry_run in when TEST is true' do
        expect(subject.options.dry_run).to eq(true)
      end
    end

    describe 'requiring additional ruby files' do
      let(:described_class) do
        Class.new(Gitlab::Triage::Engine) do
          def initialize(*args)
            # So that we can stub the instance before super is called
            yield(self)

            super
          end
        end
      end

      before do
        options.require_files = ['./path/to/ruby/file', './another/ruby/file']
      end

      it 'requires additional ruby files' do
        described_class.new(
          policies: policies,
          options: options,
          network_adapter_class:
            Gitlab::Triage::NetworkAdapters::TestAdapter) do |engine|
          expect(engine).to receive(:require).with('./path/to/ruby/file')
          expect(engine).to receive(:require).with('./another/ruby/file')
        end
      end
    end
  end

  describe '#perform' do
    it 'prints what it does to stdout' do
      expected_message = <<~MESSAGE
        Performing a dry run.

        =========================
        Triaging the `42` project
        =========================

      MESSAGE

      expect { subject.perform }.to output(expected_message).to_stdout
    end

    context 'with rules' do
      let(:policies) do
        {
          resource_rules: {
            issues: {
              rules: [
                {
                  name: 'Rule 1',
                  actions: {
                    comment: 'Hello World!'
                  }
                },
                {
                  name: 'Rule 2',
                  actions: {
                    summarize: {
                      title: "Issue title",
                      item: "- [ ] {{title}}",
                      summary: "Please triage the following new issues:\n\n{{items}}"
                    }
                  }
                }
              ]
            }
          }
        }
      end

      before do
        allow(subject.__send__(:network)).to receive(:query_api)
          .twice
          .with('https://gitlab.com/api/v4/projects/42/issues?per_page=100')
          .and_return(
            [
              { id: 1, project_id: 42, title: 'Hello', web_url: 'http://gitlab.com/project/issues/1' },
              { id: 2, project_id: 42, title: 'World', web_url: 'http://gitlab.com/project/issues/2' }
            ])
      end

      it 'prints what it does to stdout' do
        expected_message = <<~MESSAGE
          Performing a dry run.

          =========================
          Triaging the `42` project
          =========================

          ---------------------------
          Processing rules for issues
          ---------------------------

          ----------------------------------------
          Gathering resources for rule: **Rule 1**
          ----------------------------------------


          * Found 2 resources...
          * Filtering resources...
          * Total after filtering: 2 resources
          * Limiting resources...
          * Total after limiting: 2 resources

          The following comments would be posted for the rule **Rule 1**:

          # http://gitlab.com/project/issues/1
          ```
          Hello World!
          ```
          # http://gitlab.com/project/issues/2
          ```
          Hello World!
          ```

          ----------------------------------------
          Gathering resources for rule: **Rule 2**
          ----------------------------------------


          * Found 2 resources...
          * Filtering resources...
          * Total after filtering: 2 resources
          * Limiting resources...
          * Total after limiting: 2 resources

          The following issue would be created in project `42` for the rule **Rule 2**:

          >>>
          * Title: Issue title
          * Description: Please triage the following new issues:

          - [ ] Hello
          - [ ] World
          >>>


        MESSAGE

        expect { subject.perform }.to output(expected_message.chomp).to_stdout
      end

      context 'when label in actions:labels does not exist' do
        let(:policies) do
          {
            resource_rules: {
              issues: {
                rules: [
                  {
                    name: 'Rule 2',
                    actions: {
                      summarize: {
                        title: "Issue title",
                        item: "- [ ] {{title}}",
                        summary: "Please triage the following new issues:\n\n{{items}}"
                      }
                    }
                  },
                  {
                    name: 'Rule 1',
                    actions: {
                      comment: 'Hello World!',
                      labels: ['foo']
                    }
                  }
                ]
              }
            }
          }
        end

        it 'raises Gitlab::Triage::Resource::Label::LabelDoesntExistError' do
          expect(subject.__send__(:network)).to receive(:query_api_cached)
            .with('https://gitlab.com/api/v4/projects/42/labels/foo?per_page=100')
            .and_return([])

          expect { subject.perform }.to output.to_stdout.and raise_error(Gitlab::Triage::Resource::Label::LabelDoesntExistError)
        end
      end
    end

    describe 'milestone condition back-compatibility' do
      let(:policies) do
        {
          resource_rules: {
            issues: {
              rules: [
                {
                  name: 'Rule 1',
                  conditions: {
                    milestone: [
                      'v1'
                    ]
                  },
                  actions: {
                    comment: 'Hello World!'
                  }
                }
              ]
            }
          }
        }
      end

      it 'pass the correct "milestone" param to the API call' do
        expect(Gitlab::Triage::UrlBuilders::UrlBuilder).to receive(:new)
          .with(a_hash_including(params: { per_page: 100, 'milestone' => 'v1' }))
          .and_call_original

        expect { subject.perform }.to output.to_stdout
      end
    end

    describe 'summarize rules' do
      let(:policies) do
        {
          resource_rules: {
            issues: {
              summaries: [
                {
                  name: 'Summarize rule 1',
                  actions: {
                    summarize: {
                      title: 'Issue triage summary of summaries',
                      summary: "Newest and oldest issues summary:\n\n{{items}}\n\nPlease take care of them before 2018-11-27.\n\n/label ~triage-policy"
                    }
                  },
                  rules: [
                    {
                      name: 'Summarize child rule 1',
                      conditions: {
                        milestone: 'v1'
                      },
                      actions: {
                        summarize: {
                          item: "- [ ] {{title}}",
                          summary: "Please triage the following new issues:\n\n{{items}}"
                        }
                      }
                    },
                    {
                      name: 'Summarize child rule 2',
                      conditions: {
                        milestone: 'v2'
                      },
                      actions: {
                        summarize: {
                          item: "- [ ] {{title}}",
                          summary: "Please triage the following old issues:\n\n{{items}}"
                        }
                      }
                    }
                  ]
                }
              ]
            }
          }
        }
      end

      it 'pass the correct "milestone" param to the API call' do
        expect(subject.__send__(:network)).to receive(:query_api)
          .with('https://gitlab.com/api/v4/projects/42/issues?per_page=100&milestone=v1')
          .and_return([{ id: 1, project_id: 42, title: 'Hello' }, { id: 2, project_id: 42, title: 'World' }])
        expect(subject.__send__(:network)).to receive(:query_api)
          .with('https://gitlab.com/api/v4/projects/42/issues?per_page=100&milestone=v2')
          .and_return([{ id: 3, project_id: 42, title: 'Foo' }, { id: 4, project_id: 42, title: 'Bar' }])

        expected_message = <<~MESSAGE
          Performing a dry run.

          =========================
          Triaging the `42` project
          =========================

          ---------------------------
          Processing rules for issues
          ---------------------------

          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          Processing summary: **Summarize rule 1**
          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

          --------------------------------------------------------
          Gathering resources for rule: **Summarize child rule 1**
          --------------------------------------------------------


          * Found 2 resources...
          * Filtering resources...
          * Total after filtering: 2 resources
          * Limiting resources...
          * Total after limiting: 2 resources

          --------------------------------------------------------
          Gathering resources for rule: **Summarize child rule 2**
          --------------------------------------------------------


          * Found 2 resources...
          * Filtering resources...
          * Total after filtering: 2 resources
          * Limiting resources...
          * Total after limiting: 2 resources

          The following issue would be created in project `42` for the rule **Summarize rule 1**:

          >>>
          * Title: Issue triage summary of summaries
          * Description: Newest and oldest issues summary:

          Please triage the following new issues:

          - [ ] Hello
          - [ ] World

          Please triage the following old issues:

          - [ ] Foo
          - [ ] Bar

          Please take care of them before 2018-11-27.

          /label ~triage-policy
          >>>

        MESSAGE

        expect { subject.perform }.to output(expected_message).to_stdout
      end
    end

    describe 'filter by group' do
      let(:options) do
        Gitlab::Triage::Options.new.tap do |result|
          result.token = token
          result.source = 'groups'
          result.source_id = 42
        end
      end

      it 'prints what it does to stdout' do
        expected_message = <<~MESSAGE
        Performing a dry run.

        =======================
        Triaging the `42` group
        =======================


        MESSAGE

        expect { subject.perform }.to output(expected_message.chomp).to_stdout
      end
    end
  end

  describe '#resources_for_rule' do
    context 'when rule is not using GraphQL' do
      include_context 'with network context'
      include_context 'with stubs context'

      subject do
        described_class.new(
          policies: policies,
          options: network.options,
          network_adapter_class: network.adapter.class)
      end

      let(:rule) { { conditions: { weight: 'None' } } }

      before do
        allow(subject).to receive(:puts)
        allow(subject).to receive(:print)
        allow(subject.network).to receive(:print)

        stub_api(
          :get,
          "#{base_url}/projects/#{project_id}/issues",
          query: { per_page: 100, weight: 'None' },
          headers: { 'PRIVATE-TOKEN' => token }) do
          issues
        end
      end

      it 'returns the expanded resource with attached resource type' do
        subject.__send__(:resources_for_rule, 'issues', rule) do |resources|
          expect(resources.entries).not_to be_empty

          resources.each do |issue|
            expect(issue[:type]).to eq('issues')
          end
        end
      end
    end
  end

  context 'when rule is using GraphQL' do
    include_context 'with network context'
    include_context 'with stubbed graphql network context'
    include_context 'with stubs context'

    subject do
      described_class.new(
        policies: policies,
        options: network.options,
        network_adapter_class: network.adapter.class)
    end

    let(:graphql_response) { graphql_issues }
    let(:rule) { { conditions: { discussions: { attribute: 'notes', condition: 'greater_than', threshold: 10 } } } }

    before do
      allow(subject).to receive(:puts)
      allow(subject).to receive(:print)
      allow(subject.network).to receive(:print)
      allow(subject).to receive(:graphql_network).and_return(graphql_network)

      stub_api(
        :get,
        "#{base_url}/projects/#{project_id}/issues",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        issues
      end

      stub_api(
        :get,
        "#{base_url}/projects/#{project_id}",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        { 'path_with_namespace' => 'test-project' }
      end
    end

    it 'returns the expanded resource with attached resource type and decorated with GraphQL data' do
      subject.__send__(:resources_for_rule, 'issues', rule) do |resources|
        expect(resources.entries).not_to be_empty

        resources.each do |issue|
          expect(issue[:type]).to eq('issues')
          expect(issue[:user_notes_count]).to eq(15)
        end
      end
    end
  end

  context 'when rule is using GraphQL only' do
    include_context 'with network context'
    include_context 'with stubbed graphql network context'
    include_context 'with stubs context'

    subject do
      described_class.new(
        policies: policies,
        options: network.options,
        network_adapter_class: network.adapter.class)
    end

    let(:graphql_response) { graphql_issues }
    let(:rule) { { api: 'graphql', conditions: { discussions: { attribute: 'notes', condition: 'greater_than', threshold: 10 } } } }

    before do
      allow(subject).to receive(:puts)
      allow(subject).to receive(:print)
      allow(subject.network).to receive(:print)
      allow(subject).to receive(:graphql_network).and_return(graphql_network)

      stub_api(
        :get,
        "#{base_url}/projects/#{project_id}",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        { 'path_with_namespace' => 'test-project' }
      end
    end

    it 'returns the GraphQL resource with attached resource type without calling REST API to fetch issues' do
      subject.__send__(:resources_for_rule, 'issues', rule) do |resources|
        expect(resources.entries).not_to be_empty

        resources.each do |issue|
          expect(issue[:type]).to eq('issues')
          expect(issue[:user_notes_count]).to eq(15)
        end
      end
    end
  end
end
